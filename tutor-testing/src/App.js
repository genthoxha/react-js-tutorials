import React, { Component } from "react";

// LOGG in state

// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       isLoggedIn: true
//     };
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleClick() {
//     console.log("I'm working!");
//     this.setState(prevState => {
//       return {
//         isLoggedIn: !prevState.isLoggedIn
//       };
//     });
//   }

//   render() {
//     let buttonText = this.state.isLoggedIn ? "Log out" : "Log in";
//     let displayText = this.state.isLoggedIn ? "Logged in" : "Logged out";
//     return (
//       <div>
//         <button onClick={this.handleClick}>{buttonText}</button>
//         <h1>{displayText}</h1>
//       </div>
//     );
//   }
// }

// Ajax request in Starwars api

// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       loading: false,
//       character: {}
//     };
//   }

//   componentDidMount() {
//     this.setState({ loading: true });
//     fetch("https://swapi.co/api/people/2/")
//       .then(response => response.json())
//       .then(data => {
//         this.setState({
//           loading: false,
//           character: data
//         });
//       });
//   }

//   render() {
//     const text = this.state.loading ? "loading..." : this.state.character.name;
//     return (
//       <div>
//         <p>{text}</p>
//       </div>
//     );
//   }
// }

// REACT FORMS

class App extends Component {
  constructor() {
    super();
    this.state = {
      fistName: "",
      lastName: "",
      isFriendly: true,
      gender: "",
      favColor: "blue"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    // this.setState({
    //   [event.target.name]: event.target.value
    // });

    const { name, value, type, checked } = event.target;
    type === "checkbox"
      ? this.setState({ [name]: checked })
      : this.setState({ [name]: value });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          value={this.state.firstName}
          name="firstName"
          placeholder="First name"
          onChange={this.handleChange}
        />
        <br />
        <input
          type="text"
          value={this.state.lastName}
          name="lastName"
          placeholder="Last name"
          onChange={this.handleChange}
        />
        <textarea value={"Some default value"} onChange={this.handleChange} />

        <br />

        <label>
          <input
            type="checkbox"
            name="isFriendly"
            checked={this.state.isFriendly}
            onChange={this.handleChange}
          />
        </label>
        <br />
        <label>
          <input
            type="radio"
            name="gender"
            value="male"
            checked={this.state.gender === "male"}
            onChange={this.handleChange}
          />
          Male
        </label>
        <br />

        <label>
          <input
            type="radio"
            name="gender"
            value="female"
            checked={this.state.gender === "female"}
            onChange={this.handleChange}
          />
          Female
        </label>
        <br />

        <label>Favourite color: </label>
        <select
          value={this.state.favColor}
          onChange={this.handleChange}
          name="favColor"
        >
          <option value="blue">Blue</option>
          <option value="green">Green</option>
          <option value="red">Red</option>
          <option value="orange">Orange</option>
          <option value="yellow">Yellow</option>
        </select>

        {/* {Formik takes all the pain writing React forms} */}
        <h1>
          {this.state.firstName} {this.state.lastName}
        </h1>

        <h2>You are a {this.state.gender}</h2>
        <h2>Your favourite color is {this.state.favColor}</h2>
        <button onClick="">Submit</button>
      </form>
    );
  }
}

export default App;
