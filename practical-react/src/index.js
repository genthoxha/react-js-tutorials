// import React from "react";
// import ReactDOM from "react-dom";

// // JSX
// ReactDOM.render(
//   <div>
//     <h1>Hello World</h1>
//     <p>This is a paragraph</p>
//   </div>,
//   document.getElementById("root")
// );

// var myNewP = document.createElement("p");

// myNewP.innerHTML = "This is a paragraph";

////////////// divider //////// divider ///////// divider /////// divider /////////// divider //////////////// divider ///////////// divider /////////////// divider ///////

// ReactDOM.render(<MyApp />, document.getElementById("root"));

// function MyApp() {
//   return (
//     <ul>
//       <li>1</li>
//       <li>2</li>
//       <li>3</li>
//     </ul>
//   );
// }
////////////// divider //////// divider ///////// divider /////// divider /////////// divider //////////////// divider ///////////// divider /////////////// divider ///////

// should import MyInfo.js
// import MyInfo from "./components/MyInfo";

// ReactDOM.render(<MyInfo />, document.getElementById("root"));

//********************************************************************************************************************************************************************** */
////////////// divider //////// divider ///////// divider /////// divider /////////// divider //////////////// divider ///////////// divider /////////////// divider ///////

import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

ReactDOM.render(<App />, document.getElementById("root"));
