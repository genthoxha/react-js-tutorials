import React from "react";
import ReactDOM from "react-dom";

function MyInfo() {
  return (
    // This Component called ( MyInfo contains JSX below!)
    <div>
      <h1>Gent Hoxha</h1>
      <p>This is a paragraph</p>
      <ul>
        <li>Thailand</li>
        <li>Japan</li>
        <li>Nordic Countries</li>
      </ul>
    </div>
  );
}

export default MyInfo;
