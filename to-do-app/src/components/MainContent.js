import React from "react";
import ToDoItem from "./ToDoItem";
import todosData from "../todosData";

// function MainContent() {
//   // const firstName = "Bob";
//   // const lastName = "Ziroll";
//   // const date = new Date();
//   // return (
//   //   <main>
//   //     {/* <h1>Hello {firstName + lastName} </h1> */}
//   //     <h1>Hello {`${firstName} ${lastName}`}</h1>
//   //     <h1>It is currently about {date.getHours() % 12} o'clock !</h1>
//   //   </main>
//   // );
//   // const date = new Date();
//   // const hours = date.getHours();
//   // let timeOfDay;
//   // const styles = {
//   //   color: "#FF8C00",
//   //   backgroundColor: "#FF2D00",
//   //   fontSize: "200px"
//   // };
//   // if (hours < 12) {
//   //   timeOfDay = "morning";
//   //   styles.color = '#04756F'
//   // } else if (hours >= 12 && hours < 17) {
//   //   timeOfDay = "afternoon";
//   //   styles.color = "#2E0927"
//   // } else {
//   //   timeOfDay = "night";
//   //   styles.color = "#D90000"
//   // }
//   // inline styling with styles variable !
//   // return <h1 style={styles}>Good {timeOfDay}</h1>;

//   // return <div />;

//   const todoItems = todosData.map(item => <ToDoItem item={item} />);

//   return <div className="todo-list">{todoItems}</div>;
// }

class MainContent extends React.Component {
  constructor() {
    super();
    this.state = {
      todos: todosData
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(id) {
    console.log("Changed", id);
    // Update the state so that the item with the given id flips `completed` from false to true ( or vice versa)
    // Remember not to modify prevState directly, but instead to return a new version of state with the change you wanted
    // to include in that update. (Think how you might use the `.map` method to do this)

    this.setState(prevState => {
      const updatedTodos = prevState.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      });
      return {
        todos: updatedTodos
      };
    });
  }

  render() {
    const todoItems = this.state.todos.map(item => (
      <ToDoItem key={item.id} item={item} handleChange={this.handleChange} />
    ));

    return <div className="todo-list">{todoItems}</div>;
  }
}

export default MainContent;
