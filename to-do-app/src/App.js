import React from "react";

import Header from "./components/Header";
import Footer from "./components/Footer";
import MainContent from "./components/MainContent";

// functional component
// function App() {
//   return (
//     <div>
//       <Header />
//       <MainContent />
//       <Footer />
//     </div>
//   );
// }

// class component

// class App extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       isLoggedIn: false
//     };
//   }
//   render() {
//     return (
//       <div>
//         <h1>
//           You are currently {this.state.isLoggedIn ? "logged in" : "logged out"}
//         </h1>
//       </div>
//     );
//   }
// }

// export default App;

function App() {
  return (
    <div>
      <Header />
      <MainContent />
      <Footer />
    </div>
  );
}

export default App;
